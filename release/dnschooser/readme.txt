DNS Chooser，一个能帮您将域名解析成最快的IP，特别适用于CDN域名的解析。特别适合解决上coursera遇到视频不能播放，卡顿的问题，这个也是该工具的最初始动因。基本原理就是配置一组上游DNS（最多支持30个，最好不同区域都找一个，比如日本，韩国，香港。国内如果各大区域也能找一些最好），同时解析，测速后选择最快的IP返回给用户。测速的原理也简单，就是目标IP的80或443端口的建链时间。

基本功能：
	1. 智能选择最快的IP给用户
	2. 能防止DNS染污（但不能直接防止DNS劫持，特别是故意的劫持，比如故意返回假的80或443通的IP给请求都，通过特殊配置也能防止DNS支持，参见进阶信息）
	3. 本地cache，二次解析在TTL内直接返回（当前只cache了正常结果，非正常结果不会cache，也就是说如果一个域名解析得到的所有IP都是不通的，每次解析耗时都会较长，之后可能会考虑加入非正常结果的cache）
	4. 尽量做到免配置，自动对相应网卡进行配置

一些说明：
	1. 不支持TCP，当前只支持UDP
	2. 不支持IPV6
	3. 系统最低要求是Windows 7以上
	4. 缺省会将本机原有的DNS当成其中的一个上游DNS，这个过程是自动的，不需要额外管理与设置。如果不需要（假如您想将上游DNS全部配置成dnscrypt）可以强制用参数禁用（OriginalResolverAddress disable）
	5. 如果一个DNS请求没有A记录请求，则直接返回收到的第一个DNS响应
	6. 客户端在管理模式下，关闭窗口是最小到系统托盘，要真正退出客户端，请从系统托盘的菜单中退出

关于本软件的申明：
此软件为免费软件，且当前仍处于开发阶段，不能确保稳定性。愿意使用此软件的用户都被视为测试用户，本人不对用户使用此软件造成的直接或间接损失承担任何责任，如果不同意此申明，请不要使用此软件。

用户数据收集及隐私：
当前该软件只是一个客户端，不依赖于特定的服务器，您的DNS解析请求只会发送到配置里的上游DNS（及您局域网内的缺省DNS），dnschooser当前本身不会收集这些记录，但我不能确定配置里的DNS会否收集您的DNS解析请求。您也可以自己配置上游DNS。dnschooser-winclient会收集（通过Google Analytics）必要的匿名事件以用于获得必要的软件使用情况。dnschooser服务有日志功能，默认没有开启，如果您自己开启日志功能，请注意您的DNS解析记录会被全部记录。

系统要求及依赖：
	1. Windows 7（包括）以上
	2. Net Framework 4.6.1（包括）以上，请自行安装（https://www.microsoft.com/en-us/download/details.aspx?id=49982），请确定系统已经安装.NET后进行安装

安装/卸载：
	1. 当前没有做安装包，直接运行包里的install.bat安装
	2. 卸载时，请先从托盘菜单关闭dnschooser客户端程序，然后运行uninstall.bat，然后再删除，不要直接删除文件

进阶信息：
此软件是基于dnscrypt-proxy开发，尽量保留了dnscrypt-proxy的特性，可以配置上游为dnscrypt服务器，如果所有上游DNS都是dnscrypt服务器（还需要禁用自动本机DNS，配置ResolverName为多个上游dnscrypt服务器，可参考dnscrypt-proxy文档），那可以完全防止DNS劫持，但这样配置可能会导致国内一些CDN网站不能得到最好的结果。不推荐完全使用dnscrypt做为上游DNS。

FAQ：
	• Q： 我能用dnschooser科学上网么？
	• A： 抱歉，dnschooser不是用于这个目的的。

	• Q： 那这东西有什么用？
	• A： 最典型的应用场景是上coursera课程，这个也是本软件最原始的动因。通常来讲，适用于CDN域名的解析（可以理解成一个域名有多个可能的IP的）场景。因dnschooser尽量的考虑了兼容性，对于普通非CDN域名解析也适用，但没有加速作用，只是可以防止DNS污染。

	• Q： dnschooser基于80或443端口测速，那如果这些端口本身都没有打开会解析失败么？
	• A： 首先，绝大部分要解析的域名，这两个端口中的其中一个应该是打开了的。如果确实都没有打开，那dnschooser也能正常返回IP，但此IP是从上游DNS解析结果中随机选择的（即没有最快的概念的，确实没法找最快的了）。

	• Q： 安装时3x0为什么会报警？安装及配置时为什么需要管理员权限？
	• A： 该软件分两部分，一个是直接提供DNS服务的windows service，另一个是界面管理程序，安装windows service及修改网络设置在Windows下如果启用了UAC，都会要求管理员权限。如果介意，慎用。
	
包里的文件简述：
	• dnschooser.exe，主程序，以windows service的方式运行，真正提供DNS服务的组件
	• dnschooser.conf，dnschooser.exe安装时的配置文件，服务一旦安装后，修改此文件是无效的，需要重新安装
	• dnschooser-winclient.exe，界面及管理客户端，配置网卡DNS
	• install.bat，安装脚本
	• uninstall.bat，卸载脚本
	• dnscrypt-resolvers.csv，dnscrypt信息，一般用不着，除非您要配置上游DNS为dnscrypt
	• 其它，依赖库

常用安装选项：其它的可参考dnscrypt-proxy文档
	• LocalAddress(--local-address)，绑定侦听的本地地址，缺省为127.0.0.1
	• LogFile(--logfile)，日志文件，缺省不写日志
	• LogLevel(--loglevel)，日志级别，缺省是4，警告级别
	• StandardResolverAddress(--standard-resolver-address)，标准的上游DNS服务器，逗号分隔，比如”8.8.8.8:53,8.8.4.4“
	• OriginalResolverAddress(--original-resolver-address)，原始上游DNS服务器，缺省为自动获取，如果要禁用写disabled
	• BlackList(--black-list)，IP黑名单，可排除明显有问题的IP
	• LocalHosts(--local-host)，本地hosts文件，格式就为标准的hosts文件格式

BUG反馈：
https://coding.net/u/gazeboxu/p/dnschooser-pub/topic/all

感谢：
该软件使用了如下软件，在此表示感谢：
	• dnscrypt-proxy
	• dnscrypt-winclient
	• ldns
	• AutoUpdater.Net
	• GoogleAnalyticsTracker

最后，感谢您使用此软件！
Have Fun!
