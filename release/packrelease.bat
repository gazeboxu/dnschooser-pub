@echo off
set PackName=dnschooser
set ZipFile="versions\%PackName%-%1.zip"
set RootZipFile="%PackName%-latest.zip"

if "%1"=="" (
    echo you must input version string!
) else (
    copy /y changelog.txt dnschooser
    if exist %ZipFile% del %ZipFile% 
    7z a -r %ZipFile% dnschooser\*.*
    certutil -hashfile %ZipFile% md5 > %ZipFile%.md5.txt

    copy /Y %ZipFile% %RootZipFile%
)